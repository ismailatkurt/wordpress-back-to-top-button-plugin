
<?php
	// function theme_enqueue_script(){ 
		// wp_enqueue_script('jquery');  
	// }

	// add_action('wp_enqueue_scripts', 'theme_enqueue_script');
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>
jQuery(document).ready(function ($) {
	setTimeout(function() {
		$('#changes_saved').fadeOut('slow');
	}, 2000);


	$(document).on('mouseover', ".back_to_top_one_image", function () {
		$(this).css('border', '1px black solid');
	});
	$(document).on('mouseout', ".back_to_top_one_image", function () {
		if($(this).attr('id') != 'div_image_'+selectedImage.replace('.','__'))
			$(this).css('border', '1px lightgray solid');
	});
	$(document).on('click', ".back_to_top_one_image", function () {
		if($(this).parent().attr('id').replace('__','.') != selectedImage){
			$('.back_to_top_one_image').css('border', '1px lightgray solid');
			var myparent = '#div_image_'+$(this).parent().attr('id');
			$(myparent).css('border','1px black solid');
			// alert('#div_image_'+$(this).parent().attr('id'));
		}
		selectedImage = $(this).parent().attr('id').replace('__','.');
		// alert(selectedImage);
		$('#back_to_top_image').val(selectedImage);
	});
	
	$('#image_file').change(function(){
		data = new FormData($('#back_to_top_form')[0]);
        console.log('Submitting');
        $.ajax({
            type: 'POST',
            url: '../wp-content/plugins/go-to-top-button/upload-file.php',
            data: data,
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data) {
            console.log(data);
			$('#back_to_top_images').html($('#back_to_top_images').html() + data);
        }).fail(function(jqXHR,status, errorThrown) {
            console.log(errorThrown);
            console.log(jqXHR.responseText);
            console.log(jqXHR.status);
        });
	});
	
});

	function RemoveImage(myid){
		// var myid = $(this).attr('id');
		// alert(2);
		$.ajax({
			type: "post",
			url: "../wp-content/plugins/go-to-top-button/remove-file.php",
			data: {file_name: myid},
			success: function(data) {
				$('#'+myid.replace('.','__')).html('');
				$(this).remove();
				// alert('#'+myid.replace('.','///'));
				// $('.wrap').html('');
			}
		});
	}
</script>
<?php include 'functions.php'; ?>
<div class="wrap">
	<?php 
		if($_POST['hidden_field_to_check'] == 'Y'){
			update_option( 'is_active', $_POST['is_active_checkbox'] );
			update_option( 'back_to_top_bottom', $_POST['back_to_top_bottom'] );
			update_option( 'back_to_top_width', $_POST['back_to_top_width'] );
			update_option( 'back_to_top_height', $_POST['back_to_top_height'] );
			update_option( 'back_to_top_position', $_POST['back_to_top_position'] );
			update_option( 'back_to_top_image', $_POST['back_to_top_image'] );
			echo '<div id = "changes_saved"><h3 style = "color: green; text-align: right;">Changes Saved!</h3></div>';
		}
	?>
	<style>	
		.back_to_top_row {
			width: 100%;
			float: left;
			margin-top: 1em;
		}
		.back_to_top_label {
			font-weight: bold; 
			float: left; 
			width: 10em;
			height: 100%;
		}
		.back_to_top_input {
			float: left;
			width: 20em;
		}
		.back_to_top_one_image {
			float: left;
			margin: 1px;
			border: 1px lightgray solid;
		}
	</style>
	<form method="post" action="" id = "back_to_top_form">
		<input type="hidden" name="hidden_field_to_check" value="Y">
		<div><h3 style = "color: darkred;">Wordpress Back to Top Button Plugin Options Page</h3></div>
		
		<?php add_option('is_active','1','','yes'); ?>
		<?php $is_button_active = get_option( 'is_active' ); ?>
		<div class = "back_to_top_row">
			<div class = "back_to_top_label" >Is Button Active? : </div>
			<input class = "back_to_top_input" type="checkbox" style = "margin-top: 1px; " name="is_active_checkbox" value="1" <?php if($is_button_active) echo 'checked'; ?> />
		</div>
		
		<?php add_option('back_to_top_bottom','2','','yes'); ?>
		<?php $back_to_top_bottom = get_option( 'back_to_top_bottom' ); ?>
		<div class = "back_to_top_row">
			<div class = "back_to_top_label" style = "line-height: 27px;">Bottom Margin : </div>
			<input class = "back_to_top_input" type = "text" name = "back_to_top_bottom" value = "<?php echo $back_to_top_bottom; ?>">
		</div>
		
		<?php add_option('back_to_top_width','60px','','yes'); ?>
		<?php $back_to_top_width = get_option( 'back_to_top_width' ); ?>
		<div class = "back_to_top_row">
			<div class = "back_to_top_label" style = "line-height: 27px;">Image Width : </div>
			<input class = "back_to_top_input" type = "text" name = "back_to_top_width" value = "<?php echo $back_to_top_width; ?>">
		</div>
		
		<?php add_option('back_to_top_height','60px','','yes'); ?>
		<?php $back_to_top_height = get_option( 'back_to_top_height' ); ?>
		<div class = "back_to_top_row">
			<div class = "back_to_top_label" style = "line-height: 27px;">Image Height : </div>
			<input class = "back_to_top_input" type = "text" name = "back_to_top_height" value = "<?php echo $back_to_top_height; ?>">
		</div>
		
		<?php add_option('back_to_top_position','right','','yes'); ?>
		<?php $back_to_top_position = get_option( 'back_to_top_position' ); ?>
		<div class = "back_to_top_row">
			<div class = "back_to_top_label" style = "line-height: 18px;">Button Position : </div>
			<input type="radio" id="button_position" name="back_to_top_position" <?php if($back_to_top_position == 'right') echo 'checked="checked"'; ?> value="right" />Right
			<input type="radio" id="button_position" name="back_to_top_position" <?php if($back_to_top_position == 'left') echo 'checked="checked"'; ?> value="left" />Left
		</div>
		
		<?php add_option('back_to_top_image','top1.png','','yes'); ?>
		<?php $back_to_top_image = get_option( 'back_to_top_image' ); ?>
		<script>selectedImage = '<?php echo $back_to_top_image; ?>';</script>
		<div class = "back_to_top_row">
			<div class = "back_to_top_label" style = "line-height: 27px;">Select Image or Upload New : </div>
			<input class = "back_to_top_input" type = "hidden" name = "back_to_top_image" id = "back_to_top_image" value = "<?php echo $back_to_top_image; ?>">
			<input type="file" name="image_file" id="image_file"  />
		</div>
		
		<div class = "back_to_top_row" id = "back_to_top_images">
		<?php $optionsPage->PrintImages($back_to_top_image); ?>
		</div>
		
		
		<div class = "back_to_top_row"><br /></div>
		<div style = "float: right;">
			<?php submit_button(); ?>
		</div>
	</form>
</div>
