<?php
/**
 * @package Go to Top Button Plugin
 * @version 1.0
 */
/*
Plugin Name: Go to Top Button Plugin
Plugin URI: http://wordpress.org/plugins/go-to-top-button-plugin/
Description: This is a test plugin. Soon or late, it will be ready for use.
Author: İsmail Atkurt
Version: 1.0
Author URI: http://www.feelinweb.com
*/


add_action( 'admin_menu', 'register_my_custom_menu_page' );

function register_my_custom_menu_page(){
    add_menu_page( 'Back to Top', 'Back to Top', 'manage_options', 'custompage', 'back_to_top_button_page', plugins_url( 'go-to-top-button/icon.png' ), 100 ); 
}

function back_to_top_button_page(){
	include 'options_page.php';
}

include 'in-theme.php';

?>
