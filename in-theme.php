<?php

function echo_top_div(){
	echo "
	<script>
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('#back-to-top').fadeIn(duration);
			} else {
				jQuery('#back-to-top').fadeOut(duration);
			}
		});
		
		jQuery('#back-to-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})
	});
	</script>
	";
	echo '
	<style>
	#back-to-top {
		position: fixed;';
		
	$back_to_top_bottom = get_option( 'back_to_top_bottom' );
	echo 'bottom: '.$back_to_top_bottom.'em;';
	
	$back_to_top_position = get_option( 'back_to_top_position' );
	echo $back_to_top_position.': 0px;';
	
	echo 'text-decoration: none;
		color: #000000;
		font-size: 12px;
		padding: 1em;
		display: none;
		z-index: 99999;
	}

	#back-to-top:hover {    
	}
	</style>
	';
	
	$back_to_top_image = get_option( 'back_to_top_image' );
	$back_to_top_width = get_option( 'back_to_top_width' );
	if($back_to_top_image)
		echo '<a href="#" id="back-to-top"><img width = "'.$back_to_top_width.'" height = "'.$back_to_top_height.'" src = "wp-content/plugins/go-to-top-button/images/'.$back_to_top_image.'" /></a>';
	else
		echo '<a href="#" id="back-to-top">Back to Top</a>';
}
function theme_enqueue_script(){ 
    wp_enqueue_script('jquery');  
}

$is_button_active = get_option( 'is_active' );
if($is_button_active){
	add_action('wp_enqueue_scripts', 'theme_enqueue_script');
	add_action('wp_head', 'echo_top_div');
}

?>